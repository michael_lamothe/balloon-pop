package com.lamothe;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class GameState {	
	
	GameStateManager manager;
	OrthographicCamera camera;
	SpriteBatch spriteBatch;
	
	public GameState(GameStateManager manager) {
		this.manager = manager;
		camera = manager.camera;
		spriteBatch = manager.spriteBatch;
	}
	
	public abstract void render();
	public abstract void update(float dt);
}
