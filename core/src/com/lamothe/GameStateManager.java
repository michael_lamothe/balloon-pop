package com.lamothe;

import java.util.Stack;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameStateManager {

	Stack<GameState> states;
	SpriteBatch spriteBatch;
	ApplicationListener game;
	OrthographicCamera camera;
	OrthographicCamera hud;
	float accum;
	
	public final static float FPS = 60f;
	public final static float STEP = 1 / FPS;
	
	public GameStateManager(ApplicationListener game) {
		accum = 0;
		this.game = game;
		states = new Stack<GameState>();
		spriteBatch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		hud = new OrthographicCamera();
		camera.setToOrtho(false);
	}
		
	public void render() {

		if (states.empty()) {
			return;
		}
			
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		GameState state = states.peek();		
		state.render();

		accum += Gdx.graphics.getDeltaTime();
		while (accum >= STEP) {
			accum -= STEP;
			state.update(STEP);
		}
	}
	
	void pushGameState(GameState state) {
		states.push(state);
	}

	void popGameState() {
		states.pop();
	}
	
	public SpriteBatch getSpriteBatch() {
		return spriteBatch;
	}
}
