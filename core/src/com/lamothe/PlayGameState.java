package com.lamothe;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class PlayGameState extends GameState implements InputProcessor {
	
	Texture background;
	BitmapFont font;
	Sound pop;
	ArrayList<Balloon> balloons;
	ArrayList<Texture> balloonTextures;
	float balloonAccum;
	Random random;
	ArrayList<String> debug = new ArrayList<String>();
	
	final static float BALLOON_SPEED = 2.0f;
	final static float BALLOON_RATE = 2;
	final static float BALLOON_WIDTH = 135;
	final static float BALLOON_HEIGHT = 200;

	public PlayGameState(GameStateManager manager) {
		super(manager);

		Gdx.input.setInputProcessor(this);
		
		balloonAccum = BALLOON_RATE;
		background = new Texture("sky.jpg");
		pop = Gdx.audio.newSound(Gdx.files.internal("pop.mp3"));
		font = new BitmapFont();
		balloons = new ArrayList<Balloon>();
		random = new Random();
		balloonTextures = new ArrayList<Texture>();

		loadBalloon("aqua");
		loadBalloon("brown");
		loadBalloon("darkblue");
		loadBalloon("grey");
		loadBalloon("green");
		loadBalloon("lightblue");
		loadBalloon("orange");
		loadBalloon("pink");
		loadBalloon("purple");
		loadBalloon("red");
		loadBalloon("white");
		loadBalloon("yellow");
	}

	private void loadBalloon(String colour) {
		balloonTextures.add(new Texture("balloons/" + colour + ".png"));
	}
		
	public void render() {
		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.begin();
		spriteBatch.draw(background, 0, 0);

		for (int i = 0; i < balloons.size(); i++) {
			Balloon balloon = balloons.get(i);
			spriteBatch.draw(balloon.texture, balloon.x, balloon.y);
		}
				
		int y = Gdx.graphics.getHeight();
		for (int i = 0; i < debug.size(); i++) {
			font.draw(spriteBatch, String.format("%s", debug.get(i)), 10, y - 20*i);
		}
		
		spriteBatch.end();
	}
	
	public void update(float dt) {

		// Balloons float up
		for (int i = 0; i < balloons.size(); i++) {
			Balloon balloon = balloons.get(i);
			balloon.y += BALLOON_SPEED;

			if (balloon.y > (Gdx.graphics.getHeight() + BALLOON_HEIGHT)) {
				balloons.remove(i);
			}
		}
		
		// Add a new balloon
		balloonAccum += dt;
		if (balloonAccum > BALLOON_RATE) {
			int index = random.nextInt(balloonTextures.size());			
			float screenWidth = Gdx.graphics.getWidth() - (2 * BALLOON_WIDTH);
			
			Balloon balloon = new Balloon();
			balloon.x = (random.nextFloat() * screenWidth) + BALLOON_WIDTH;
			balloon.y = -BALLOON_HEIGHT;
			balloon.texture = balloonTextures.get(index);
			balloons.add(balloon);
			
			balloonAccum = 0;
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.ESCAPE) {
			Gdx.app.exit();
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	static boolean in(int x, float start, float end) {
		return start <= x && end >= x;
	}
	
	static boolean in(int x, int y, Balloon balloon) {
		return in(x, balloon.x, balloon.x + BALLOON_WIDTH) &&
				in(y, balloon.y, balloon.y + BALLOON_HEIGHT);
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		int y = Gdx.graphics.getHeight() - screenY;
		for (int i = 0; i < balloons.size(); i++) {
			Balloon balloon = balloons.get(i);
			
			if (in(screenX, y, balloon)) {
				pop.play();
				balloons.remove(i);
			}
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
