package com.lamothe;

import com.badlogic.gdx.ApplicationAdapter;

public class BalloonPop extends ApplicationAdapter {

	GameStateManager manager;
	
	@Override
	public void create () {
		manager = new GameStateManager(this);		
		manager.pushGameState(new PlayGameState(manager));
	}

	@Override
	public void render () {
		if (manager.states.empty()) {
			dispose();
			return;
		}

		manager.render();
	}
}
